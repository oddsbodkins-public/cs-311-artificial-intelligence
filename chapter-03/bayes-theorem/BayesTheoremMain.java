/*
	Understanding Bayes Theorem.

	Terms:

	P(A|B)	- Probability of A "given" B
	P(A)	- Probability of A
	P(B)	- Probability of B
	P(B|A)	- Probability of B "given" A

	Bayes Theorem Formula:

	P(A|B) = [ P(A) * P(B|A) ] / P(B)

	Example: 
	
	There are two bowls of poker chips.
	
	Bowl 1: 20 Red chips and 20 Blue chips
	Bowl 2: 30 Red chips and 10 Blue chips

	If I pick a random bowl, what is the probability that I pick a red chip from Bowl 2?

	P(A|B) = ?
	P(Bowl 2 | given red chip)

	---------------
	Solution:

	What is the probability of picking a red chip from Bowl 1?
	
	20 Red chips / 20 red + 20 blue = 20 / 40 = 0.5

	Can be written as:

	P(red | Bowl1) - Probability of red "given" Bowl 1
	P(red | Bowl1) = 0.5

		or

	P(B | A) = 0.5

	Event B = pick red chip
	Even A = pick bowl 1

	
	What is the probability of picking a red chip from Bowl 2?

	30 Red chips / 30 red + 10 blue = 30 / 40 = 0.75

	or written as:

	P(red | Bowl2) = 0.75

		or

	P(B | A) = 0.75

	Event B = pick red chip
	Even A = pick bowl 2


	What is the probability of picking Bowl 1?

	1 Bowl / 2 Bowls total (Bowl 1 & Bowl 2) = 0.5

	P(A) = 0.5

	What is the probability of picking Bowl 2?

	1 Bowl / 2 Bowls total (Bowl 1 & Bowl 2) = 0.5	

	P(A) = 0.5


	What is the probability of picking a red chip out of all the chips?

	P(red) = P(Bowl 1) * P(red | Bowl 1) + P(Bowl 2) * P(red | Bowl 2)

	Use above calculations:

	Bowl 1 and red chip from Bowl 1:
	0.5 * 0.5 = 0.25

	Bowl 2 and red chip from Bowl 2:
	0.5 * 0.75 = 0.375

	Add together:
	0.25 + 0.375 = 0.625

	P(B) = 0.625 (picking a red chip from either bowl)



	Rewrite Bayes Theorem:

	P(A|B) = [ P(A) * P(B|A) ] / P(B)

	Substitute values:

	Event A = P(A) = Pick Bowl 2
	Event B = P(B) = Pick red chip
	P(A | B) = pick bowl 2 given red chip
	P(B | A) = pick red chip given bowl 2

	P(A) = 0.5
	P(B|A) = 0.75
	P(B) = 0.625


	Result:

	P(A|B) = [ 0.5 * 0.75] / 0.625 = 0.6

	The probability of picking a red chip from bowl 2 is 0.6 or 60%

	The bottom of Bayes Theorem can be written like we did for calculating the probability of picking a red chip out of either bowl.

	P(A) = P(Bowl 1) * P(red | Bowl 1) + P(Bowl 2) * P(red | Bowl 2) + ... P(Bowl n) * P(red | Bowl n)

	for as many bowls as we want!


	so we rewrite Bayes Theorem:

	P(A[k] | B) = ( P(A[k]) * P(B | A[k]) ) / SUM: ( P(A[i] * P(B | A[i]) ) 

	k - subscript (pick red chip, pick blue chip, pick green chip, ...)
	i - subscript (bowl 1, bowl 2, bowl 3, ...)

	In other examples the probability can be improved by using the result of the first iteration in the second iteration. 
*/


/*
	This program will use Bayes Theorem to calculate a probability for a dice roll.

	We have 5 die:

	1 - 4  sided dice
	1 - 6  sided dice
	1 - 8  sided dice
	1 - 12 sided dice
	1 - 20 sided dice 

	I pick a random die and tell you the roll result. You guess what dice was used. 
	I will do multiple rolls, but not change what dice is being used. With each roll the probability of the guess improves.

*/

// https://study.com/academy/lesson/using-bayes-theorem-in-ai-decision-making.html

import java.util.concurrent.ThreadLocalRandom;
import java.util.Formatter;

public class BayesTheoremMain {

	private static final int FOUR_SIDED_DIE = 1;
	private static final int SIX_SIDED_DIE = 2;
	private static final int EIGHT_SIDED_DIE = 3;
	private static final int TWELVE_SIDED_DIE = 4;
	private static final int TWENTY_SIDED_DIE = 5;

	private int _currentSelectedDie = -1;

	/*
		For debugging
	*/
	private void printCurrentDie() {

		switch(this._currentSelectedDie) {

			case FOUR_SIDED_DIE:
				System.out.println("Picked 4-sided dice");
				break;

			case SIX_SIDED_DIE:
				System.out.println("Picked 6-sided dice");
				break;

			case EIGHT_SIDED_DIE:
				System.out.println("Picked 8-sided dice");
				break;

			case TWELVE_SIDED_DIE:
				System.out.println("Picked 12-sided dice");
				break;

			case TWENTY_SIDED_DIE:
				System.out.println("Picked 20-sided dice");
				break;

			default:
				throw new ArithmeticException("Invalid Die Number!");
		}
	}

	/*
		Pick random dice.
	*/
	private void pickRandomDie () {
		// nextInt is normally exclusive of the top value, add 1 to make it inclusive
		this._currentSelectedDie = ThreadLocalRandom.current().nextInt (FOUR_SIDED_DIE, TWENTY_SIDED_DIE + 1);
	}

	private int rollDie () {
		int min = 1;
		int max = -1;
		int currentRoll = -1;

		switch(this._currentSelectedDie) {

			case FOUR_SIDED_DIE:
				max = 4;
				break;

			case SIX_SIDED_DIE:
				max = 6;
				break;

			case EIGHT_SIDED_DIE:
				max = 8;
				break;

			case TWELVE_SIDED_DIE:
				max = 12;
				break;

			case TWENTY_SIDED_DIE:
				max = 20;
				break;

			default:
				throw new ArithmeticException("Invalid Die Number!");
		}

		// nextInt is normally exclusive of the top value, add 1 to make it inclusive
		currentRoll = ThreadLocalRandom.current().nextInt (min, max + 1);

		return currentRoll;
	}

	private double [] printFirstRollProbabilityTable (int currentRoll) {
		int dieArray [] = {4, 6, 8, 12, 20};

		// 5 die total, probability = 1 / 5 = 0.2
		double dieProbabilityOriginal = 0.2;

		// this is modified based on the roll (Ex: impossible to roll a '12' with '6' sided die)
		double dieProbabilityMultipliedByRollGivenDie [] = {0, 0, 0, 0, 0};
		double sumOfDieProbabilityMultipliedByRollGivenDie = 0;

		// calculated later
		double dieGivenRollProbability [] = {0, 0, 0, 0, 0};
		double sumOfDieGivenRollProbability = 0;

		// According to dieArray [] order
		// 4-sided die has 1-4, so probability is 1 / 4 = 0.25
		// 6-sided die has 1-6, so probability is 1 / 6 = 0.166667
		// ...
		double rollProbabilityForEachDie [] = { 1d / 4d, 1d / 6d, 1d / 8d, 1d / 12d, 1d / 20d };
  
		/*
			Modify probability, is the current roll out of range for a given dice?

			Example: 

			Current roll = 6
			Cannot be 4-sided die, because only has 1-4 values. Probability of 4-sided die = 0.
		*/

		if (currentRoll > 12) {
			rollProbabilityForEachDie [0] = rollProbabilityForEachDie [1] = rollProbabilityForEachDie [2] = rollProbabilityForEachDie [3] = 0;
		} else if (currentRoll > 8 && currentRoll <= 12) {
			rollProbabilityForEachDie [0] = rollProbabilityForEachDie [1] = rollProbabilityForEachDie [2] = 0;
		} else if (currentRoll > 6 && currentRoll <= 8) {
			rollProbabilityForEachDie [0] = rollProbabilityForEachDie [1] = 0;
		} else if (currentRoll > 4) {
			rollProbabilityForEachDie [0] = 0;
		}

		/*
			Multiply the probability of picking a die: 'dieProbabilityOriginal' by the probability of each roll 'dieProbabilityMultipliedByRollGivenDie'

			Update the probabilities, (Ex: impossible to roll a '12' with '6' sided die)

		*/
		for (int a = 0; a < dieProbabilityMultipliedByRollGivenDie.length; a++) {
			// update one probability array
			dieProbabilityMultipliedByRollGivenDie [a] = (double) rollProbabilityForEachDie [a] * dieProbabilityOriginal;

			// add all the probabilities together to use in Bayes Theorem calculation
			sumOfDieProbabilityMultipliedByRollGivenDie += dieProbabilityMultipliedByRollGivenDie [a];
		}

		for (int b = 0; b < dieProbabilityMultipliedByRollGivenDie.length; b++) {
			dieGivenRollProbability [b] = (double) dieProbabilityMultipliedByRollGivenDie[b] / sumOfDieProbabilityMultipliedByRollGivenDie;

			// add all above entries together
			sumOfDieGivenRollProbability += dieGivenRollProbability [b];
		}

		// Prepare column names for string table
		String rollGivenDieTitle = String.format ("P(roll=%d | Die[k])", currentRoll);
		String columnThreeTimesFourTitle = String.format ("P(Die[k]) * %s", rollGivenDieTitle);
		String dieGivenRollTitle = String.format ("P(Die[k] | roll=%d)", currentRoll);

		ConsoleStringTable table = new ConsoleStringTable ();

		table.addString(0, 0, " k ");
		table.addString(0, 1, "Die[k]");
		table.addString(0, 2, "P(Die[k])");
		table.addString(0, 3, rollGivenDieTitle);
		table.addString(0, 4, columnThreeTimesFourTitle);
		table.addString(0, 5, dieGivenRollTitle);

		// print row name separator
		table.addString(1, 0, table.column (" k "));
		table.addString(1, 1, table.column ("Die[k]"));
		table.addString(1, 2, table.column ("P(Die[k])"));
		table.addString(1, 3, table.column (rollGivenDieTitle));
		table.addString(1, 4, table.column (columnThreeTimesFourTitle));
		table.addString(1, 5, table.column (dieGivenRollTitle));

		for (int i = 0; i < dieArray.length; i++) {
			table.addString(i + 2, 0, String.format("%d", (i + 1)) );
			table.addString(i + 2, 1, String.format("%d", dieArray[i]) );
			table.addString(i + 2, 2, String.format("%f", dieProbabilityOriginal) );
			table.addString(i + 2, 3, String.format("%f", rollProbabilityForEachDie[i]) );
			table.addString(i + 2, 4, String.format("%f", dieProbabilityMultipliedByRollGivenDie [i]) );
			table.addString(i + 2,5, String.format("%f", dieGivenRollProbability [i]) );
		}

		// Add addition row
		table.addString (7, 0, "Sum");
		table.addString (7, 4, String.format ("%f", sumOfDieProbabilityMultipliedByRollGivenDie));
		table.addString (7, 5, String.format ("%f", sumOfDieGivenRollProbability));

		System.out.println(table.toString());

		return dieGivenRollProbability;
	}

	/*
		Second roll uses probability from the first roll, calculates a few figures for new events, and then uses the Bayes Theorem again. 
	*/
	private double [] printSecondRollProbabilityTable (int currentRoll, double [] firstRollProbabilities) {

		int dieArray [] = {4, 6, 8, 12, 20};

		double rollProbabilityForEachDie [] = { 1d / 4d, 1d / 6d, 1d / 8d, 1d / 12d, 1d / 20d };
		double dieProbabilityMultipliedByRollGivenDie [] = {0, 0, 0, 0, 0};
		double dieGivenRollProbability [] = {0, 0, 0, 0, 0};
		double sumOfDieProbabilityMultipliedByRollGivenDie = 0;
		double sumOfDieGivenRollProbability = 0;

		if (currentRoll > 12) {
			rollProbabilityForEachDie [0] = rollProbabilityForEachDie [1] = rollProbabilityForEachDie [2] = rollProbabilityForEachDie [3] = 0;
		} else if (currentRoll > 8 && currentRoll <= 12) {
			rollProbabilityForEachDie [0] = rollProbabilityForEachDie [1] = rollProbabilityForEachDie [2] = 0;
		} else if (currentRoll > 6 && currentRoll <= 8) {
			rollProbabilityForEachDie [0] = rollProbabilityForEachDie [1] = 0;
		} else if (currentRoll > 4) {
			rollProbabilityForEachDie [0] = 0;
		}

		for (int a = 0; a < dieProbabilityMultipliedByRollGivenDie.length; a++) {
			// update one probability array
			dieProbabilityMultipliedByRollGivenDie [a] = (double) rollProbabilityForEachDie [a] * firstRollProbabilities [a];

			// add all the probabilities together to use in Bayes Theorem calculation
			sumOfDieProbabilityMultipliedByRollGivenDie += dieProbabilityMultipliedByRollGivenDie [a];
		}

		for (int b = 0; b < dieProbabilityMultipliedByRollGivenDie.length; b++) {
			dieGivenRollProbability [b] = (double) dieProbabilityMultipliedByRollGivenDie[b] / sumOfDieProbabilityMultipliedByRollGivenDie;

			// add all above entries together
			sumOfDieGivenRollProbability += dieGivenRollProbability [b];
		}

		String rollGivenDieTitle = String.format ("P(roll=%d | Die[k])", currentRoll);
		String columnThreeTimesFourTitle = String.format ("P(Die[k]) * %s", rollGivenDieTitle);
		String dieGivenRollTitle = String.format ("P(Die[k] | roll=%d)", currentRoll);

		ConsoleStringTable table = new ConsoleStringTable ();

		table.addString(0, 0, " k ");
		table.addString(0, 1, "Die[k]");
		table.addString(0, 2, "P(Die[k])");
		table.addString(0, 3, rollGivenDieTitle);
		table.addString(0, 4, columnThreeTimesFourTitle);
		table.addString(0, 5, dieGivenRollTitle);


		// print row name separator
		table.addString(1, 0, table.column (" k "));
		table.addString(1, 1, table.column ("Die[k]"));
		table.addString(1, 2, table.column ("P(Die[k])"));
		table.addString(1, 3, table.column (rollGivenDieTitle));
		table.addString(1, 4, table.column (columnThreeTimesFourTitle));
		table.addString(1, 5, table.column (dieGivenRollTitle));

		for (int i = 0; i < dieArray.length; i++) {
			table.addString(i + 2, 0, String.format("%d", (i + 1)) );
			table.addString(i + 2, 1, String.format("%d", dieArray [i]) );
			table.addString(i + 2, 2, String.format("%f", firstRollProbabilities [i]) );
			table.addString(i + 2, 3, String.format("%f", rollProbabilityForEachDie[i]) );
			table.addString(i + 2, 4, String.format("%f", dieProbabilityMultipliedByRollGivenDie [i]) );
			table.addString(i + 2, 5, String.format("%f", dieGivenRollProbability [i]) );
		}

		// Add addition row
		table.addString (7, 0, "Sum");
		table.addString (7, 4, String.format ("%f", sumOfDieProbabilityMultipliedByRollGivenDie));
		table.addString (7, 5, String.format ("%f", sumOfDieGivenRollProbability));

		System.out.println(table.toString());

		return null;
	}


	public static void main (String [] args) {
		BayesTheoremMain btm = new BayesTheoremMain(); 
		btm.pickRandomDie();
		//btm.printCurrentDie();

		System.out.printf ("\n");

		int roll = btm.rollDie();

		System.out.printf("First Roll: %d\n\n", roll);

		double [] firstRollProbabilities = btm.printFirstRollProbabilityTable(roll);
	
		System.out.printf("\n\n");

		roll = btm.rollDie();

		System.out.printf("Second Roll: %d\n\n", roll);

		btm.printSecondRollProbabilityTable (roll, firstRollProbabilities);
	}
}
