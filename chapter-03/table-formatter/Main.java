public class Main {
	public static void main (String [] args) {
		ConsoleStringTable table = new ConsoleStringTable();

		table.addString(0, 0, "AveryVeryVeryLongWord");
		table.addString(0, 1, "AnotherWord");
		table.addString(0, 2, "Foo");
		table.addString(0, 3, "Bar");

		table.addString(1, 0, "---------------------");
		table.addString(1, 1, "------------");
		table.addString(1, 2, "---");
		table.addString(1, 3, "---");

		table.addString(2, 0, "1");
		table.addString(2, 1, "2");
		table.addString(2, 3, "3");
		table.addString(3, 2, "3");

		System.out.println(table.toString());
	}
}
