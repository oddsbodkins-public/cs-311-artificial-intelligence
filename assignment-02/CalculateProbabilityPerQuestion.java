import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.HashMap;

public class CalculateProbabilityPerQuestion {
	/*
		Probability for the first question, because no survey answers have been input.

		From the Pew Research Center
		"What the 2020 electorate looks like by party, race and ethnicity, age, education and religion"
		By John Gramlich
		Written: 2020-Oct-26
		Accessed: 2022-Feb-27
	 
		https://www.pewresearch.org/fact-tank/2020/10/26/what-the-2020-electorate-looks-like-by-party-race-and-ethnicity-age-education-and-religion/

		0.34 - Independent
		0.33 - Democrat
		0.04 - Other
		0.29 - Republican
	*/
	private double _democrat = 0.33;
	private double _independent = 0.34;
	private double _other = 0.04;
	private double _republican = 0.29;

	public CalculateProbabilityPerQuestion () {
		_loadDataFiles ();
	}


	public void printProbability () {
		// From USA, gathered from 2020 data / articles
		System.out.println ("\nProbability of Political Party Identification");
		System.out.println ("---------------------------------------------");
		System.out.printf ("Democrat:\t%f\n", this._democrat);
		System.out.printf ("Independent:\t%f\n", this._independent);
		System.out.printf ("Other:\t\t%f\n", this._other);
		System.out.printf ("Republican:\t%f\n\n", this._republican);
	}

	private void _loadDataFiles () {
		String [] questionTotals = new String [10];
		String [] dataFiles = {"./collected_data/democrat_party.txt", "./collected_data/independent_party.txt", "./collected_data/other_party.txt", "./collected_data/republican_party.txt"};

		for (int b = 0; b < dataFiles.length; b++) {
		 
			try {
				File file = new File (dataFiles [b]);
				Scanner scan = new Scanner (file);

				int fileLine = 1;

				QuestionTracker [] qt = new QuestionTracker [10];
				for (int a = 0; a < 10; a++) {
					qt [a] = new QuestionTracker (a);
				}

				while (scan.hasNextLine ()) {
					// skip the first line, column identification string
					if (fileLine == 1) {
						scan.nextLine ();
					} else if (fileLine == 2) { // skip the second line, totals for each question
						String line = scan.nextLine ();
						questionTotals = line.split (",");
					} else {
						String line = scan.nextLine ();
						String [] questionAnswers = line.split (",");

						for (int i = 0; i < questionAnswers.length; i++) {
							qt [i].addAnswer (questionAnswers [i], dataFiles [b]);
							qt [i].addTotalOptions (questionTotals [i]);
						}
					}

					fileLine += 1;
				}

				/*
				qt [0].printDemocratTotals ();
				qt [0].printIndependentTotals ();
				qt [0].printOtherTotals ();
				qt [0].printRepublicanTotals ();
				*/

				scan.close ();
			} catch (FileNotFoundException e) {
				System.out.println ("An error occurred!");
				e.printStackTrace ();
			}

		}

	}

	private class QuestionTracker {
		private int _questionNumber = -1;
		private int _questionTotalOptions = -1;
		private HashMap <String, Integer> _democratAnswerTotals;
		private HashMap <String, Integer> _independentAnswerTotals;
		private HashMap <String, Integer> _otherAnswerTotals;
		private HashMap <String, Integer> _republicanAnswerTotals;

		public QuestionTracker (int q) {
			this._questionNumber = q;
			this._democratAnswerTotals = new HashMap <String, Integer> ();
			this._independentAnswerTotals = new HashMap <String, Integer> ();
			this._otherAnswerTotals = new HashMap <String, Integer> ();
			this._republicanAnswerTotals = new HashMap <String, Integer> ();
		}

		public void addTotalOptions (String s) {
			this._questionTotalOptions = Integer.parseInt (s);
		}

		public void addAnswer (String s, String fileName) {
			switch (fileName) {
				case "./collected_data/democrat_party.txt":
					addDemocratAnswer (s);
					break;

				case "./collected_data/independent_party.txt":
					addIndependentAnswer (s);
					break;

				case "./collected_data/other_party.txt":
					addOtherAnswer (s);
					break;

				case "./collected_data/republican_party.txt":
					addRepublicanAnswer (s);
					break;
			}
		}

		public void addDemocratAnswer (String s) {
			// check if any previous answer totals exist
			if (this._democratAnswerTotals.containsKey (s)) {
				int i = this._democratAnswerTotals.get (s);
				i += 1;
				this._democratAnswerTotals.put (s, i);
			} else {
				this._democratAnswerTotals.put (s, 1);
			}
		}

		public void addIndependentAnswer (String s) {
			// check if any previous answer totals exist
			if (this._independentAnswerTotals.containsKey (s)) {
				int i = this._independentAnswerTotals.get (s);
				i += 1;
				this._independentAnswerTotals.put (s, i);
			} else {
				this._independentAnswerTotals.put (s, 1);
			}
		}

		public void addOtherAnswer (String s) {
			// check if any previous answer totals exist
			if (this._otherAnswerTotals.containsKey (s)) {
				int i = this._otherAnswerTotals.get (s);
				i += 1;
				this._otherAnswerTotals.put (s, i);
			} else {
				this._otherAnswerTotals.put (s, 1);
			}
		}

		public void addRepublicanAnswer (String s) {
			// check if any previous answer totals exist
			if (this._republicanAnswerTotals.containsKey (s)) {
				int i = this._republicanAnswerTotals.get (s);
				i += 1;
				this._republicanAnswerTotals.put (s, i);
			} else {
				this._republicanAnswerTotals.put (s, 1);
			}
		}

		public void printDemocratTotals () {
			String s = "";
			for (String key : this._democratAnswerTotals.keySet ()) {
				s += String.format ("Total Options: %d, Question %d: Key '%s': Value: '%d'\n", this._questionTotalOptions, this._questionNumber, key, this._democratAnswerTotals.get (key));
			}
			System.out.println (s);
		}

		public void printRepublicanTotals () {
			String s = "";
			for (String key : this._republicanAnswerTotals.keySet ()) {
				s += String.format ("Total Options: %d, Question %d: Key '%s': Value: '%d'\n", this._questionTotalOptions, this._questionNumber, key, this._republicanAnswerTotals.get (key));
			}
			System.out.println (s);
		}

		public void printIndependentTotals () {
			String s = "";
			for (String key : this._independentAnswerTotals.keySet ()) {
				s += String.format ("Total Options: %d, Question %d: Key '%s': Value: '%d'\n", this._questionTotalOptions, this._questionNumber, key, this._independentAnswerTotals.get (key));
			}
			System.out.println (s);
		}

		public void printOtherTotals () {
			String s = "";
			for (String key : this._otherAnswerTotals.keySet ()) {
				s += String.format ("Total Options: %d, Question %d: Key '%s': Value: '%d'\n", this._questionTotalOptions, this._questionNumber, key, this._otherAnswerTotals.get (key));
			}
			System.out.println (s);
		}
	}
}
