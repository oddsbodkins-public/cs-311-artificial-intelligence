/**
* CS 311 - Artificial Intelligence
* Assignment 02: AI Survey Program
* 2022-Feb-24
*
*
* @version 0.1
* @since   2022-02-24 
*/

/*
	TODO: 

			1. Finish (Dice) Bayes Theorem program.
			2. Import or create random data to use. (10 rows per political party)
			3. Use Bayes Theorem program to predict answers that match.
*/

/*
	Political data / Questions from:

	https://electionstudies.org/wp-content/uploads/2021/07/anes_timeseries_2020_questionnaire_20210719.pdf

	American National Election Studies. THE ANES GUIDE TO PUBLIC OPINION AND ELECTORAL BEHAVIOR. August 16, 2021 version. 
	
	https://electionstudies.org/resources/anes-guide/
*/

		/*
			"Do you think that people in government waste a lot of money?", {"Waste a lot", "Waste some", "Do not waste very much"} - V201235
			"How many of the people running the government are corrupt?", {"All", "Most", "About half", "A few", "None"} - V201236
			"Generally speaking, how often can you trust other people?", {"Always", "Most of the time", "About half the time", "Some of the time", "Never"} - V201237
			"How much do you feel that having elections makes the government pay attention to what the people think?", {"A good deal", "Some", "Not much"} - V201238
			"Which party do you think would do a better job of handling the nation’s economy?", {"Democrats would do a much better", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job", "Republicans would do a much better job"} - V201239
			"Which party do you think would do a better job of handling health care?", {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"} - V201240
			"Which party do you think would do a better job of handling immigration?", {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"} - V201241
			"Which party do you think would do a better job of handling taxes?",  {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"} - V201242
			"Which party do you think would do a better job of handling the environment?", {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"} - V201243
			"Which party do you think would do a better job of handling the COVID-19 pandemic?", {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"} - V201244
			-----------------------
			"What POLITICAL PARTY are you registered with, if any?" - V201018

		*/

import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class PoliticalSurvey {

	private CalculateProbabilityPerQuestion _cppq;

	/**
	* Print an introduction and instructions to the user.
	* @return Void.
	*/
	private void _printIntroduction () {
		System.out.println ("Political Survey");
		System.out.println ("----------------");
	}

	/**
	* Function used to get user input. Loops until a valid option is selected.
	* @return Void.
	*/
	public void getUserResponses (Scanner scan) {

/*
	SELECT V201235, V201236, V201237, V201238, V201239, V201240, V201241, V201242, V201243, V201244 FROM anes_timeseries_2020_csv_20220210 WHERE V201018 = 1 LIMIT 10; // Democrat

	SELECT V201235, V201236, V201237, V201238, V201239, V201240, V201241, V201242, V201243, V201244 FROM anes_timeseries_2020_csv_20220210 WHERE V201018 = 2 LIMIT 10; // Republican

	SELECT V201235, V201236, V201237, V201238, V201239, V201240, V201241, V201242, V201243, V201244 FROM anes_timeseries_2020_csv_20220210 WHERE V201018 = 4 LIMIT 10; // Independent

	SELECT V201235, V201236, V201237, V201238, V201239, V201240, V201241, V201242, V201243, V201244 FROM anes_timeseries_2020_csv_20220210 WHERE V201018 = 5 LIMIT 10; // Other

-----------------

V201235 - DOES GOVERNMENT WASTE MUCH TAX MONEY

1. Waste a lot
2. Waste some
3. Don’t waste very much

-----------------------

V201236 - HOW MANY IN GOVERNMENT ARE CORRUPT

1. All
2. Most
3. About half
4. A few
5. None

------------------

V201237 - HOW OFTEN CAN PEOPLE BE TRUSTED

1. Always
2. Most of the time
3. About half the time
4. Some of the time
5. Never

-----------------

V201238 - ELECTIONS MAKE GOVERNMENT PAY ATTENTION

1. A good deal
2. Some
3. Not much

-----------------

V201239 - WHICH PARTY BETTER: HANDLING NATIONS ECONOMY

1. Democrats would do a much better job
2. Democrats would do a somewhat better job
3. Not much difference between them
4. Republicans would do a somewhat better job
5. Republicans would do a much better job

-----------------

V201240 - WHICH PARTY BETTER: HANDLING HEALTH CARE

1. Democrats would do a much better job
2. Democrats would do a somewhat better job
3. Not much difference between them
4. Republicans would do a somewhat better job
5. Republicans would do a much better job

------------------

V201241 - WHICH PARTY BETTER: HANDLING IMMIGRATION

1. Democrats would do a much better job
2. Democrats would do a somewhat better job
3. Not much difference between them
4. Republicans would do a somewhat better job
5. Republicans would do a much better job

------------------

V201242 - WHICH PARTY BETTER: HANDLING TAXES

1. Democrats would do a much better job
2. Democrats would do a somewhat better job
3. Not much difference between them
4. Republicans would do a somewhat better job
5. Republicans would do a much better job

-----------------

V201243 - WHICH PARTY BETTER: HANDLING ENVIRONMENT

1. Democrats would do a much better job
2. Democrats would do a somewhat better job
3. Not much difference between them
4. Republicans would do a somewhat better job
5. Republicans would do a much better job

------------------

V201244 - WHICH PARTY BETTER: HANDLING COVID-19

1. Democrats would do a much better job
2. Democrats would do a somewhat better job
3. Not much difference between them
4. Republicans would do a somewhat better job
5. Republicans would do a much better job

------------------

V201018 (Party identification)

1. Democratic party
2. Republican party
4. None or ‘independent’
5. Other {SPECIFY}

*/
		SurveyHelper [] list = {
			new SurveyHelper ("Do you think that people in government waste a lot of money?", new String [] {"Waste a lot", "Waste some", "Do not waste very much"}, "V201235", scan),
			new SurveyHelper ("How many of the people running the government are corrupt?", new String [] {"All", "Most", "About half", "A few", "None"}, "V201236", scan),
			new SurveyHelper ("Generally speaking, how often can you trust other people?", new String [] {"Always", "Most of the time", "About half the time", "Some of the time", "Never"}, "V201237", scan),
			new SurveyHelper ("How much do you feel that having elections makes the government pay attention to what the people think?", new String [] {"A good deal", "Some", "Not much" }, "V201238", scan),
			new SurveyHelper ("Which party do you think would do a better job of handling the nation’s economy?", 
				new String [] {"Democrats would do a much better", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job", "Republicans would do a much better job"}, "V201239", scan),
			new SurveyHelper ("Which party do you think would do a better job of handling health care?", 
				new String [] {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"}, "V201240", scan),
			new SurveyHelper ("Which party do you think would do a better job of handling immigration?", 
				new String [] {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"}, "V201241", scan),
			new SurveyHelper ("Which party do you think would do a better job of handling taxes?", 
				new String []  {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"}, "V201242", scan),
			new SurveyHelper ("Which party do you think would do a better job of handling the environment?", 
				new String [] {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"}, "V201243", scan),
			new SurveyHelper ("Which party do you think would do a better job of handling the COVID-19 pandemic?", 
				new String [] {"Democrats would do a much better job", "Democrats would do a somewhat better job", "Not much difference between them", "Republicans would do a somewhat better job.", "Republicans would do a much better job"}, "V201244", scan)
		};

		// Strings used to write to a file
		String columnNames = "";
		String numberOfOptions = "";
		String selectionValues = "";

		for (int i = 0; i < list.length; i++) {
			this._cppq.printProbability ();

			if (i == 0) {
				this._printIntroduction ();
			}

			list [i].printQuestionAndGetInput (i);
			
			list [i].printSelection ();
			numberOfOptions += String.format ("%d,", list [i].getNumberOfOptions ());
			columnNames += list [i].getIdentifier () + ",";

			// Selection values need to be incremented to match database values.
			// If loaded from the file, decrement them.
			selectionValues += (list [i].getSelection () +  1) + ",";
		}

		columnNames = columnNames.substring (0, columnNames.length () - 1);
		numberOfOptions = numberOfOptions.substring (0, numberOfOptions.length () - 1);
		selectionValues = selectionValues.substring (0, selectionValues.length () - 1);

		System.out.println ("\nLast Question");
		System.out.println ("\n-------------");

		SurveyHelper partySelection = new SurveyHelper ("With which political party are you registered? (Listed alphabetically).", new String [] {"Democrat", "Independent", "Other", "Republican"}, "V201018", scan);
		partySelection.printQuestionAndGetInput (0);

		// Select what file to write
		String fileName = "";

		switch (partySelection.getSelection ()) {
			case 0: // Democrat
				fileName = "./collected_data/democrat_party.txt";
				break;

			case 1: // Independent
				fileName = "./collected_data/independent_party.txt";
				break;

			case 2: // Other
				fileName = "./collected_data/other_party.txt";
				break;

			case 3: // Republican
				fileName = "./collected_data/republican_party.txt";
				break;
		}

		System.out.println (columnNames);
		System.out.println (numberOfOptions);

		try {
			File file = new File(fileName);
			// verify that parent directory exists
			file.getParentFile ().mkdirs ();

			if (file.createNewFile()) {
				// file was created, file.getName ()
				// include the column names

				FileWriter fw = new FileWriter (file, true);
				fw.write (columnNames + "\n" + numberOfOptions + "\n" + selectionValues + "\n");
				fw.close ();
			} else {
				// file already exists, append new data to the end
				FileWriter fw = new FileWriter (file, true);
				fw.write (selectionValues + "\n");
				fw.close ();
			}

		} catch (IOException e) {
			System.out.println ("A file error occurred!");
			e.printStackTrace ();
		}

	}

	/**
	* PoliticalSurvey class constructor.
	* @return Void.
	*/
	public PoliticalSurvey () {
		this._cppq = new CalculateProbabilityPerQuestion ();

		Scanner scan = new Scanner (System.in);

		getUserResponses (scan);

		// free scanner memory
		scan.close ();
	}

	/**
	* Main method of PoliticalSurvey class.
	* @param args Unused.
	* @return Void.
	*/
	public static void main (String [] args) {
		PoliticalSurvey ps = new PoliticalSurvey ();
	}
}
