import java.util.Scanner;

/*
	Helper class. Stores the question string, options array, and selection.
*/

public class SurveyHelper {
	private String _question;
	private String [] _options;
	private String _identifier;
	private int _selection = -1;
	private Scanner _scanner;

	public SurveyHelper (String question, String [] options, String identifier, Scanner scanner) {
		this._question = question;
		this._options = options;
		this._identifier = identifier;
		this._scanner = scanner;
	}

	public void setSelection (int s) {
		if (s >= 0 && s < this._options.length) {
			this._selection = s;
		}
	}

	public String getQuestion () {
		return this._question;
	}

	public String [] getOptions () {
		return this._options;
	}

	public int getNumberOfOptions () {
		return this._options.length;
	}

	public String getIdentifier () {
		return this._identifier;
	}

	public int getSelection () {
		return this._selection;
	}

	/**
	* Given an array of string options, print the selection. Useful for debugging.
	* @return Void.
	*/

	public void printSelection () {
		System.out.printf ("You selected '%s'\n\n", this._options [this._selection]);
	}

	/**
	* Given an array of strings, print them in a numbered list. This allows the user to input an option.
	* Loops until a valid selection is input.
	*
	* @param instance of SurveyHelper, It contains the question string, options list, and selection.
	* @return Void.
	*/

	public void printQuestionAndGetInput (int questionNumber) {
		System.out.printf ("Question #%d: %s\n", questionNumber + 1, this._question);
		for (int i = 0; i < this._options.length; i++) {
			System.out.printf ("%d: %s\n", (i + 1), this._options [i]);
		}

		// Scanner input loop
		while (true) {
			System.out.printf ("Selection: (1-%d): ", this._options.length);

			// verify that the input is an integer
			if (this._scanner.hasNext ()) {
				if (this._scanner.hasNextInt ()) {
					int i = this._scanner.nextInt ();

					// the integer must be between 1 and X (X = however many options available)
					if (i < 1 || i > this._options.length) {
						continue;
					}

					this.setSelection (i - 1);
					break;
				} else { // not an integer, ignore the input and repeat the loop
					this._scanner.next ();
				}
			}
		}
	}
}
