#/bin/bash
g++ -c -o learner.o learner.cpp
g++ -c -o showmessage.o showmessage.cpp
g++ -c -o getphrase.o getphrase.cpp
g++ -c -o main.o main.cpp
g++ -o main main.o getphrase.o showmessage.o learner.o -lm
